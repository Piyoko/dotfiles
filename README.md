# noaccOS' Linux Rice
Git repository containing my dotfiles in order to have coherent behaviour between my desktop pc and my laptop.  
![greeting](.config/neofetch/greeter.png "Shitsurei, kamimashita 🐌")

## Awesome
My daily choice, swapped from i3 gaps and really happy about it

My awesome rice contains two themes

| Name | Description                                                                                                                                            | Picture                                              |  
| ---  | -------------------------------------------------------------------------------                                                                        | ----------------------------------------             |  
| Fox  | My first "real" rice.  Has awesome colors, terminal theme, gtk-theme (in `.config/oomox`) and [telegram](https://t.me/addtheme/dy3q98gjaQaBxoQE) theme | ![fox-pic](.dotfiles-pictures/fox.png "Fox rice")    |  
| Nord | A nord-based theme.  Colors for terminal, custom prompt, awesome colors and custom telegram theme.                                                     | ![nord-pic](.dotfiles-pictures/nord.png "Nord rice") |  

The theme I'm using right now and is preconfigured is nord  
You can use it out of the box and it should work as expected for terminal applications. The prompt is [here](https://github.com/noaccOS/pure) until merged  
To theme GUI programs you can use these themes:
- [GTK Theme](https://github.com/EliverLara/Nordic)
- [Telegram Theme](https://github.com/noaccOS/telegram-nord-theme)
- [Firefox css](https://github.com/ralphSQ/Nord-minimal-functional-fox)
- [Firefox theme](https://addons.mozilla.org/en-US/firefox/addon/noaccos-nord/)
- [Discord](https://gist.github.com/dhilln/f74098c730659cd7d9fd4e4e15f5b8cd)

### Dependencies
To get the most out of the rice, you're adviced to get all the required dependencies  
The list is for Arch Linux packages, the links are git links so you can search informations for your distribution 
 
| Arch name (embedded git link)                                    | Usage / Description                     | Comment                                                                                                                                                                                                                         |
| -----------------------------                                    | -------------------                     | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [`awesome-git`](https://github.com/awesomeWM/awesome)            | The window manager itself               | The git version is recommended to avoid icon errors in the bar                                                                                                                                                                  |
| [`rofi`](https://github.com/davatorium/rofi)                     | Application launcher and emoji selector | Still searching for the right emoji selector though                                                                                                                                                                             |
| [`kitty`](https://github.com/kovidgoyal/kitty)                   | Terminal emulator                       | If you want to use something else, be sure to import the [colors](.config/kitty/colors.conf)                                                                                                                                    |
| [`fish`](https://github.com/fish-shell/fish-shell)               | Shell                                   | You can find my theme [here](https://github.com/noaccOS/pure) (**INSTALLATION INSTRUCTION DON'T WORK ATM**) but be sure to return to the original one after it's rewritten so you can achieve the same without a forked version |
| [`neofetch`](https://github.com/dylanaraps/neofetch)             | Terminal greeter                        | Display informations about the system                                                                                                                                                                                           |
| [`xfce-polkit`](https://github.com/ncopa/xfce-polkit)            | Polkit agent                            | Allows programs to run with elevated privileges (you can use another if you prefer but be sure to launch it from rc.lua)                                                                                                        |
| [`nerd-fonts-complete`](https://github.com/ryanoasis/nerd-fonts) | Nerd patched fonts                      | The solution I've found to render the workspaces as intended is to download the whole package. `nerd-fonts-icons` and `nerd-fonts-jetbrains-mono` suffice but the icons are bigger then intended                                |

### Warning
Some applications are set to always open in certain tags and/or monitors  
If you experience errors, consider editing/removing the lines in `.config/awesome/rc.lua`  

``` lua
-- Tag rules
{ rule = { instance = "telegram-desktop"},
  properties = {screen = 2, tag = "1"}},
{ rule = { class = "discord"},
  properties = {screen = 2, tag = "2"}},
{ rule = { class = "[sS]potify"},
  properties = {screen = 2}},
```
Also, some applications are set to **autostart**  
They are launched from the last line in `rc.lua` (`autostart()`)

#### Autostart function
It doesn't work es expected, the first and second argument for now do the same thing, in future I hope to fix that  

| Argument                 | What it does                                                                            |  
| --------                 | ------------                                                                            |  
| First argument  (string) | The program to launch with focus                                                        |  
| Second argument (table)  | Launch the other programs, without steling the focus from the first (rn they do though) |  
| Third argument  (table)  | Launch programs with a shell, useful for more complex commands like the polkit          |  

## Todos
- [ ] Enable laptop stuff only on laptop (Should do a different branch)
    - [ ] Brightness
    - [ ] Battery
    - [ ] Network Manager
    - [ ] Keyboard backlight
    - [ ] Libinput gestures
- [x] End 'Fox' Rice
    - [x] Change wibar color
    - [x] Change workspace identifiers with dots
    - [x] Fish colors
    - [x] Swap button and bar button colors
- [ ] Import (and update) install instructions into git
- [x] ~~Disable compton for fullscreen apps~~ Removed compton
- [ ] Setp volnoti
- [x] Awesome
    - [x] Automatic tag assignement for specific apps (spotify, telegram, discord)
- [ ] Make this markdown an org
