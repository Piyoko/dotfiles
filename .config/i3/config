set $mod  Mod4
set $term kitty

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8
font pango:Fira Code 11

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec $term 

#bindsym $mod+Shift+q kill
bindsym $mod+space exec rofi -show drun

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+i focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Up    focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+i move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+Shift+t layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+Shift+f focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window using arrows
bindsym $mod+Ctrl+Left  resize shrink width  10 px or 10 ppt
bindsym $mod+Ctrl+Up    resize grow   height 10 px or 10 ppt
bindsym $mod+Ctrl+Down  resize shrink height 10 px or 10 ppt
bindsym $mod+Ctrl+Right resize grow   width  10 px or 10 ppt

# resize windows using keys
bindsym $mod+Ctrl+h resize shrink width  10 px or 10 ppt
bindsym $mod+Ctrl+k resize grow   height 10 px or 10 ppt
bindsym $mod+Ctrl+j resize shrink height 10 px or 10 ppt
bindsym $mod+Ctrl+l resize grow   width  10 px or 10 ppt

# custom codes
gaps inner 15
gaps outer 15

new_window 1pixel

exec_always --no-startup-id setxkbmap -layout us -variant altgr-intl
exec_always --no-startup-id $HOME/.config/polybar/launch
exec_always --no-startup-id nitrogen --restore
exec_always --no-startup-id "killall -q compton; compton"
exec_always --no-startup-id "xset s off; xset -dpms"
#exec_always --no-startup-id pacmd set-default-sink 0
exec_always --no-startup-id pacmd set-source-volume 0 65536

## multimedia keys
# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +2% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -2% #decrease sound volume
bindsym XF86AudioMute        exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound

# Media player controls
bindsym XF86AudioPlay 	exec playerctl play-pause
bindsym XF86AudioPause 	exec playerctl play-pause
bindsym XF86AudioNext 	exec playerctl next
bindsym XF86AudioPrev 	exec playerctl previous

# custom keybindings
bindsym $mod+c 	    	exec google-chrome-stable
bindsym $mod+t 	    	exec telegram-desktop
bindsym $mod+d		exec discord-canary
bindsym $mod+s		exec LD_PRELOAD=/usr/lib/spotify-adblock.so spotify
bindsym $mod+r 	    	exec $term -e ranger
bindsym Print       	exec flameshot full -c
bindsym Ctrl+Print  	exec flameshot full -p $HOME/Pictures/Screenshots/
bindsym $mod+Shift+s 	exec flameshot gui # -p $HOME/Pictures/Screenshots/
bindsym $mod+Print  	exec flameshot screen -r -c
bindsym $mod+Shift+p 	exec poweroff

# Fibonacci layout
default_orientation vertical 			#start in opposite orientation from your monitor
for_window [class=".*"] split toggle 	#toggles split at each new window
bindsym $mod+shift+q "split toggle; kill"  #kill command resets the split
