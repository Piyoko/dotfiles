function ls
    set --local num (/bin/ls $argv | wc -l)
    if test $num -le 10
        lsd -A1 $argv
    else
        lsd -A $argv
    end
end
