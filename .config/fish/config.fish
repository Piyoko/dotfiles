alias em='emacsclient -c'
alias emt='emacsclient -t'
alias vim='emacsclient -t'

# Yay command
alias yup='yay -Syu --overwrite \*'
alias yin='yay -S --overwrite \*'

# Commands redefinition
alias clear='clear;neofetch'
alias rm='rm -r'
